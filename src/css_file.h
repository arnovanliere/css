#ifndef CSS_FILE_H
#define CSS_FILE_H

#include <string>
#include <tuple>
#include <vector>

const int VAR_NAME = 0;
const int VAR_VALUE = 1;

class CSSFile {
public:
    // Default constructor
    CSSFile();

    // Specify filename and try to read that file immediately
    // and parse the variables if reading succeeds
    // @param 'filename'          : Name of the CSS-file
    // @param 'filename_variables': Name of the CSS-file where the variables are defined
    // @param 'filename_output'   : Name of the CSS-file where the output is written to
    CSSFile(char const *filename, char const *filename_variables, char const *filename_output);

    // Destructor
    ~CSSFile();

    // Read CSS-file
    // @param 'name'   : Name of the CSS-file
    // @param 'content': Content of file is saved in 'content'
    // @return         : 'true' if reading succeeded, 'false' otherwise
    static bool read(char const *name, std::string &content);

    // Parse the variables in the CSS-file
    void parse_variables();

    // Replace the variables in the CSS-file with the corresponding values
    void replace_variables();

    void replace_nested_variables();

    // Print stats
    // @param 'vars'        : Whether to show the variables which are found in the 'filename_variables'
    // @param 'replacements': Whether to show the number of replacements of a certain variable
    void print_stats(bool vars, bool replacements) const;


private:
    // Name of the CSS-file
    char const *filename;
    // Name of the CSS-file with variables
    char const *filename_variables;
    // Name of the CSS-file to write output to
    char const *filename_output;
    // Contents of the CSS-file where the variables are used
    std::string file_contents;
    // Contents of the CSS-file where the variables are defined
    std::string file_contents_variables;
    // Tuple contains 'name', 'value', 'replacements'
    std::vector<std::tuple<std::string, std::string, int>> variables;

    // Trim spaces at both ends of a string
    // @param 's': The string to be trimmed
    // @return   : The trimmed string (same as 's')
    static std::string trim(std::string &s);

    // Get max-width for a column
    // @param 'index'  : One of the constants to determine which column
    // @param 'padding': Amount of spaces right of the column
    // @return         : Width of the column
    int max_width(int index, int padding) const;
};

#endif