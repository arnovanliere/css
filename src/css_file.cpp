#include "css_file.h"
#include <algorithm>
#include <cctype>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <tuple>

CSSFile::CSSFile() {
    filename = nullptr;
    filename_variables = nullptr;
    filename_output = nullptr;
    file_contents = "";
    file_contents_variables = "";
    variables = {};
}

CSSFile::CSSFile(char const *filename, char const *filename_variables, char const *filename_output) {
    this->filename = filename;
    this->filename_variables = filename_variables;
    this->filename_output = filename_output;
    if (read(filename_variables, file_contents_variables) && read(filename, file_contents))
        parse_variables();
}

CSSFile::~CSSFile() = default;

bool CSSFile::read(char const *name, std::string &content) {
    if (name == nullptr) {
        std::cerr << "ERROR: Invalid filename" << std::endl;
        return false;
    }

    std::ifstream file(name, std::ios::in);
    if (file.fail()) {
        std::cerr << "ERROR: \"" << name << "\" could not be opened" << std::endl;
        return false;
    }

    content.assign((std::istreambuf_iterator<char>(file)), (std::istreambuf_iterator<char>()));

    return true;
}

void CSSFile::parse_variables() {
    char input_char, prev_char = '\0', next_char;
    int file_size = static_cast<int>(file_contents_variables.size()) - 1;
    std::string variable_name, variable_value;
    int variable = 0, index = 0;

    input_char = file_contents_variables[index];
    while (index < file_size) {
        next_char = file_contents_variables[index + 1];
        // Variable-names start with '--' and end with a ':'
        // 'prev_char' should be a space, tab or linebreak to prevent
        // variable use to be recognised as variable declaration
        if (input_char == '-' && next_char == '-' &&
            (prev_char == ' ' || prev_char == '\n' || prev_char == '\t')) {
            // Reset 'variable_name' and 'variable_value'
            variable_name = "";
            variable_value = "";
            while (input_char != ':' && index != file_size) {
                variable_name += input_char;
                index++;
                input_char = file_contents_variables[index];
            }
            // Skip all spaces and the ':' after 'variable_name'
            while ((input_char == ' ' || input_char == ':') && index != file_size) {
                index++;
                input_char = file_contents_variables[index];
            }
            // Only after a variable-name follows a variable-value
            // Variable-value ends with ';'
            while (input_char != ';' && index != file_size) {
                variable_value += input_char;
                index++;
                input_char = file_contents_variables[index];
            }
            auto tuple = std::make_tuple(trim(variable_name), trim(variable_value), variable);
            variables.push_back(tuple);
            // Increase counter for the variables
            variable++;
        }
        // Increase index for reading the file-contents
        index++;
        prev_char = input_char;
        input_char = file_contents_variables[index];
    }
}

void CSSFile::replace_variables() {
    std::string line, variable_name, variable_value;
    int number_replacements;
    unsigned long long int pos;
    std::string output_contents = file_contents;

    std::ofstream fileout(filename_output);
    for (auto &variable : variables) {
        number_replacements = 0;
        variable_name = "var(" + std::get<0>(variable) + ")";
        variable_value = std::get<1>(variable);
        // Find first occurrence of the variable_name
        pos = output_contents.find(variable_name);
        while (pos != std::string::npos) {
            number_replacements++;
            // Replace 'variable_name' string with 'variable_value'
            output_contents.replace(pos, variable_name.length(), variable_value);
            // Continue searching from here
            pos = output_contents.find(variable_name, pos);
        }
        std::get<2>(variable) = number_replacements;
    }
    fileout << output_contents;
}

void CSSFile::replace_nested_variables() {
    // TODO
}

void CSSFile::print_stats(bool vars, bool replacements) const {
    if (variables.empty())
        std::cerr << "ERROR: No variables found in \"" << filename_variables << "\"" << std::endl;
    else
        std::cout << variables.size() << " variables found in \"" << filename_variables << "\"" << std::endl;

    if (vars) {
        std::cout << std::endl << "The following variables were found in \"" << filename_variables
                  << "\" and replaced in \"" << filename << "\"" << std::endl;
        std::cout << std::left << std::setw(max_width(VAR_NAME, 2)) << "Name"
                  << std::left << std::setw(max_width(VAR_VALUE, 2)) << "Value";
    }

    if (vars && replacements) {
        std::cout << "Replacements" << std::endl;
        for (auto variable : variables)
            std::cout << std::left << std::setw(max_width(VAR_NAME, 2)) << std::get<0>(variable)
                      << std::left << std::setw(max_width(VAR_VALUE, 2)) << std::get<1>(variable)
                      << std::get<2>(variable) << std::endl;
    } else if (vars) {
        std::cout << std::endl;
        for (auto variable : variables)
            std::cout << std::left << std::setw(max_width(VAR_NAME, 2)) << std::get<0>(variable)
                      << std::left << std::setw(max_width(VAR_VALUE, 2)) << std::get<1>(variable) << std::endl;
    }
    std::cout << std::endl << "Output written to: \"" << filename_output << "\"" << std::endl;
}

// Private functions
std::string CSSFile::trim(std::string &s) {
    s.erase(s.begin(), find_if(s.begin(), s.end(), not1(std::ptr_fun<int, int>(isspace))));
    s.erase(find_if(s.rbegin(), s.rend(), not1(std::ptr_fun<int, int>(isspace))).base(), s.end());
    return s;
}

int CSSFile::max_width(int index, int padding) const {
    int max = (index == VAR_NAME) ? 4 : 5; // 4 for 'Name', 5 for 'Value'
    if (index != VAR_NAME && index != VAR_VALUE)
        return -1;

    for (auto variable : variables) {
        if (index == VAR_NAME && static_cast<int>(std::get<0>(variable).size()) > max)
            max = std::get<0>(variable).size();
        else if (index == VAR_VALUE && static_cast<int>(std::get<1>(variable).size()) > max)
            max = std::get<1>(variable).size();
    }

    return max + padding;
}
