#ifndef ARG_PARSE_H
#define ARG_PARSE_H

#include <string>
#include <tuple>
#include <vector>

class ArgParse {
public:
    // Constructor
    ArgParse();
    // Constructor with info to show in 'help'
    // @param 'name'   : Name of the executable
    // @param 'license': License under which the software is released
    // @param 'creator': Creator of the software
    ArgParse(std::string name, std::string license, std::string creator);
    // Constructor with info to show in 'help'
    // @param 'name'   : Name of the executable
    // @param 'license': License under which the software is released
    // @param 'year'   : Year in which the copyright started
    // @param 'creator': Creator of the software
    ArgParse(std::string name, std::string license, std::string year, std::string creator);

    // Destructor
    ~ArgParse();

    // Add argument, bound to position
    // @param 'value'   : Pointer to the parameter of type 'char' which saves the value provided for this argument
    // @param 'arg_name': Name of the argument
    // @param 'help'    : Description of the argument
    void add_arg(std::string *value, std::string const &arg_name, std::string const &help);

    // Add option
    // @param 'value'       : Pointer to parameter of type 'bool' in which
    //                        should be saved whether option was provided or not
    // @param 'full_option' : Full name of the option, prefix: --
    // @param 'short_option': Short name of the option, prefix: -
    // @param 'help'        : Description of the option
    void add_option(bool *value, std::string const &full_option, std::string const &short_option, std::string const &help);

    // Parse options according to the options added with 'add_option'
    // @param 'argc': Number of options
    // @param 'argv': Array with the options provided
    // @return      : 'true' if all required arguments were provided, false otherwise
    bool parse_args(int argc, char *argv[]);

    // Print help according to the options added with 'add_option'
    // @param 'argv': Array with arguments, used if constructor without 'name' was provided
    void help(char *argv[]) const;

private:
    std::string name;    // Name of the executable
    std::string creator; // Creator of the software
    std::string year;    // Year of publication
    std::string license; // License under which software is released
    // Tuple contains: 'value', 'name', 'help'
    std::vector<std::tuple<std::string*, std::string, std::string>> arguments;
    // Tuple contains: 'value', 'full_option', 'short_option', 'help'
    std::vector<std::tuple<bool*, std::string, std::string, std::string>> options;

    // Get specific option
    // @return: Tuple (containing: 'full_option', 'short_option', 'help')
    //          where 'full_option' = 'option' or 'short_option' = option
    std::tuple<bool*, std::string, std::string, std::string> get_option(std::string const &option) const;
    // Check if a specific option is provided in 'argv'
    // @param 'full_option' : Full name of the argument to check for
    // @param 'short_option': Short name of the argument to check for
    // @param 'argc'        : The number of options in 'argv'
    // @param 'argv'        : Provided options
    // @return              : 'true' if option is present in 'argv', 'false' otherwise
    static bool contains_option(std::string const &full_option, std::string const &short_option, int argc, char **argv);
    // Check if an option was added with 'add_option'
    // @param 'option': Name (full or short) of the option to check for
    // @return        : 'true' if option was added, 'false' otherwise
    bool option_added(std::string const &option) const;
    // Get the width for the column with argument-/option-name
    // @param 'padding': How much whitespace right of the column
    // @return         : Width of the column (with the padding)
    int get_width(int padding) const;
};

#include "arg_parse.cpp"

#endif