#include "arg_parse.h"
#include <ctime>
#include <iomanip>
#include <iostream>

ArgParse::ArgParse() {
    options.emplace_back(nullptr, "help", "h", "Print this message");
}

ArgParse::ArgParse(std::string name, std::string license, std::string creator) {
    if (!name.empty() && !license.empty() && !creator.empty()) {
        this->name = std::move(name);
        this->license = std::move(license);
        this->year = "";
        this->creator = std::move(creator);
    }
}

ArgParse::ArgParse(std::string name, std::string license, std::string year, std::string creator) {
    if (!name.empty() && !license.empty() && !year.empty() && !creator.empty()) {
        this->name = std::move(name);
        this->license = std::move(license);
        this->year = std::move(year);
        this->creator = std::move(creator);
    }
}

ArgParse::~ArgParse() {
    for (auto argument : arguments)
        delete std::get<0>(argument);
    for (auto option : options)
        delete std::get<0>(option);
}

void ArgParse::add_arg(std::string *value, std::string const &arg_name, std::string const &help) {
    if (value == nullptr)
        throw std::invalid_argument("'value' is a null-pointer");
    else
        arguments.emplace_back(value, arg_name, help);
}

void ArgParse::add_option(bool *value, std::string const &full_option, std::string const &short_option, std::string const &help) {
    if (value == nullptr)
        throw std::invalid_argument("'value' is a null-pointer");
    else
        options.emplace_back(value, full_option, short_option, help);
}

bool ArgParse::parse_args(int argc, char *argv[]) {
    int nr_required = static_cast<int>(arguments.size());

    if (contains_option("help", "h", argc, argv)) {
        help(argv);
        return false;
    }

    if ((argc - 1) < nr_required) {
        std::cerr << "ERROR: Not all required parameters provided" << std::endl << std::endl;
        help(argv);
        return false;
    }

    // Start at '1' to skip the name of the executable
    for (int i = 1; i <= nr_required; i++)
        *std::get<0>(arguments[i - 1]) = argv[i];

    for (int i = nr_required + 1; i < argc; i++) {
        if (option_added(argv[i]))
            *std::get<0>(get_option(argv[i])) = true;
        else
            std::cerr << "ERROR: Unknown option " << argv[i] << std::endl << std::endl;
    }

    return true;
}

void ArgParse::help(char *argv[]) const {
    std::cout << "Usage: ";
    if (!name.empty())
        std::cout << name << " ";
    else if (argv != nullptr)
        std::cout << argv[0] << " ";

    if (!arguments.empty()) {
        for (auto argument : arguments)
            std::cout << "<" << std::get<1>(argument) << "> ";
    }

    std::cout << std::endl << "[OPTIONAL]" << std::endl;

    if (!arguments.empty()) {
        std::cout << "REQUIRED ARGUMENTS" << std::endl;
        for (auto argument : arguments)
            std::cout << "    "
                      << std::left
                      << std::setw(get_width(2))
                      << std::get<1>(argument)
                      << std::get<2>(argument)
                      << std::endl;
        std::cout << std::endl;
    }

    if (!options.empty()) {
        std::cout << "OPTIONAL PARAMETERS" << std::endl;
        for (auto option : options)
            std::cout << "    "
                      << std::left
                      << std::setw(get_width(2))
                      << "--" + std::get<1>(option) + ", -" + std::get<2>(option)
                      << std::get<3>(option) << std::endl;
    }

    if (!license.empty() && !creator.empty()) {
        std::cout << std::endl;
        std::time_t time = std::time(nullptr);
        std::tm *now = std::localtime(&time);
        std::cout << "Creator: " << creator << " | License: " << license;
        if (!year.empty())
            std::cout << " | Copyright: " << (now->tm_year + 1900) << " - "  << year;
        std::cout << std::endl;
    }
}

std::tuple<bool*, std::string, std::string, std::string> ArgParse::get_option(std::string const &option) const {
    for (auto const &opt : options)
        if ("--" + std::get<1>(opt) == option || "-" + std::get<2>(opt) == option)
            return opt;
    return std::tuple<bool*, std::string, std::string, std::string>(nullptr, "", "", "");
}

bool ArgParse::contains_option(std::string const &full_option, std::string const &short_option, int argc, char **argv) {
    for (int i = 1; i < argc; i++)
        if (argv[i] == "--" + full_option || argv[i] == "-" + short_option)
            return true;

    return false;
}

bool ArgParse::option_added(std::string const &option) const {
    for (auto const &opt : options)
        if ("--" + std::get<1>(opt) == option || "-" + std::get<2>(opt) == option)
            return true;

    return false;
}

int ArgParse::get_width(int padding) const {
    int max = 0;
    for (auto const &argument : arguments)
        if (static_cast<int>(std::get<1>(argument).length()) > max)
            max = static_cast<int>(std::get<1>(argument).length());
    for (auto const &option : options)
        // + 5 for ', ' and '--' and '-'
        if (static_cast<int>(std::get<1>(option).length() + std::get<2>(option).length()) + 5 > max)
            max = static_cast<int>(std::get<1>(option).length() + std::get<2>(option).length()) + 5;

    return max + padding;
}