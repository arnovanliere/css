#include <algorithm>
#include <string>
#include "src/css_file.h"
#include "include/arg_parse.h"

int main(int argc, char *argv[]) {
    auto *input_file = new std::string, *input_variables_file = new std::string, *output_file = new std::string;
    bool *nested = new bool, *replacements = new bool, *variables = new bool, parse_success;

    ArgParse *parser;
    CSSFile *cssFile;

    parser = new ArgParse("css", "MIT", "Arno van Liere");
    parser->add_arg(input_file, "CSS-file", "File in which the CSS-variables are used");
    parser->add_arg(input_variables_file, "CSS-file w/ variables", "File in which the css variables are declared");
    parser->add_arg(output_file, "output filename", "Name of the file to which the output should be written");
    parser->add_option(nested, "nested", "n", "Replace nested variables");
    parser->add_option(replacements, "replacements", "r", "Show number of replacements");
    parser->add_option(variables, "variables", "v", "Show variables which are found in variables file");
    parse_success = parser->parse_args(argc, argv);
    if (!parse_success)
        return 1;

    cssFile = new CSSFile(input_file->c_str(), input_variables_file->c_str(), output_file->c_str());
    if (*nested)
        cssFile->replace_nested_variables();
    else
        cssFile->replace_variables();
    cssFile->print_stats(*variables, *replacements);
}
